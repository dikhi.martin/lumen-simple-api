# Set the base image for subsequent instructions
FROM php:8.2-fpm

# Set the working directory inside the container
WORKDIR /app

# Copy the project files to the container's working directory
COPY ./project /app

# Update packages and install dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        git \
        cron \
        libzip-dev \
        libjpeg-dev \
        libpng-dev \
        libfreetype6-dev \
        libbz2-dev \
        libpq-dev \
        default-mysql-client \ 
    && rm -rf /var/lib/apt/lists/*

# Install needed extensions
RUN docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install pdo_mysql zip gd bz2 

# Setup a cron schedule
RUN echo "* * * * * cd /app && /usr/local/bin/php artisan schedule:run >> /var/log/cron.log 2>&1" | crontab -

# Redirect cron output to a log file
RUN touch /var/log/cron.log

# Install Composer
RUN curl --silent --show-error --location "https://getcomposer.org/installer" | php -- --install-dir=/usr/local/bin --filename=composer

# Install Laravel Envoy (optional, you can skip this if not needed)
RUN composer global require "laravel/envoy=~1.0"

# Create the necessary directories before changing their ownership
RUN mkdir -p /app/storage /app/bootstrap/cache

# Set permissions to allow the container user to write to the directories
RUN chown -R www-data:www-data /app/storage

# Install Composer dependencies and make sure the "vendor" directory is inside /app
RUN composer install

# Expose port 9000
EXPOSE 9000

# Create an entry point script
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Define the entry point for your container
ENTRYPOINT ["/entrypoint.sh"]
