#!/bin/sh

# Running composer if vendor doesnt exists
composer install

# Run migrations
php artisan migrate --seed

# Generate documentation
php artisan swagger-lume:generate

# Running cron 
service cron start

# Generate passport
php artisan passport:install --no-interaction

# Start the Laravel application
php artisan serve --host=0.0.0.0 --port=9000