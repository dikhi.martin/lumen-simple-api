# Lumen Sample API

## Tech Stack:

- Docker Engine: [Docker Engine Installation Guide](https://docs.docker.com/engine/install)
- Docker Compose: [Docker Compose Installation Guide](https://docs.docker.com/compose/install)

## Docker Method

```sh
cp project/.env.example project/.env
```

```sh
docker-compose up -d --build
```

### Database Preparation

```sh
docker-compose exec app php artisan migrate
```

```sh
docker-compose exec app php artisan db:seed
```

### Generate passport with no interaction

```sh
docker-compose exec app php artisan passport:install --no-interaction
```

### Generate passport 

```sh
docker-compose exec app php artisan passport:install --force
```

### After everything is done:

Run this command to view passport and grant:

```sh
docker-compose logs app
```

The output will be similar to this:

```sh
lumen-simple-api-app-1  | Encryption keys generated successfully.
lumen-simple-api-app-1  | Personal access client created successfully.
lumen-simple-api-app-1  | Client ID: 1
lumen-simple-api-app-1  | Client secret: QO5BCAYevc4RVt4hprOLz27n2seicBfA7ewWX9v0
lumen-simple-api-app-1  | Password grant client created successfully.
lumen-simple-api-app-1  | Client ID: 2
lumen-simple-api-app-1  | Client secret: A1bHbKinckztwKVoxDAdgfiQrqkhU6FU7WKWUvsF
```

Copy the Client ID and Client Secret from the output above.

In the `.env` file, paste these values into the appropriate variables:

```sh
PASSPORT_PERSONAL_ACCESS_CLIENT_ID=1
PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET=QO5BCAYevc4RVt4hprOLz27n2seicBfA7ewWX9v0

PASSPORT_PASSWORD_GRANT_CLIENT_ID=2
PASSPORT_PASSWORD_GRANT_CLIENT_SECRET=A1bHbKinckztwKVoxDAdgfiQrqkhU6FU7WKWUvsF
```

Make sure to save your changes in the `.env` file after pasting these values. This will ensure that your passport configuration is properly set up for your application.

### Check cron log

```sh
docker-compose exec app cat /var/log/cron.log
```

### Show Route list

```sh
docker-compose exec app php artisan route:list
```

## How to generate Documentation Swagger

- Run `php artisan swagger-lume:publish-config` to publish configs (`config/swagger-lume.php`)
- Run `php artisan swagger-lume:publish-views` to publish views (`resources/views/vendor/swagger-lume`)
- Run `php artisan swagger-lume:publish` to publish everything
- Run `php artisan swagger-lume:generate` to generate docs

```
http://localhost:8000/api/documentation
```

## Magic Development Usage

- Create Migration `php artisan make:migration create_{table_name}_table`
- Create Model & Migration at once `php artisan make:model {ModelName} -m`

## Passport 

- Purge expired token `php artisan passport:purge`

## Official Documentation

- Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).
- Generate interactive OpenAPI documentation for your RESTful API using doctrine annotations.
  [swagger-php](https://github.com/zircote/swagger-php)
- Build Eloquent queries from API requests
  [Query Builder](https://spatie.be/docs/laravel-query-builder/v5/introduction)

## Contributing

Thank you for considering contributing to Lumen! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).