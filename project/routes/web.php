<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/', function () use ($router) {
    return $router->app->version();
});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['namespace'=>'App\Http\Controllers','middleware'=>['token']],function($api){
        // register
        $api->post('/register','UserController@register');

        // products
        $api->post('/products','ProductController@create');
        $api->get('/products','ProductController@index');
        $api->get('/products/{id}','ProductController@show');
        $api->put('/products/{id}','ProductController@update');
        $api->delete('/products/{id}','ProductController@destroy');
        
        // assets
        $api->post('/assets','AssetsController@create');
        $api->get('/assets','AssetsController@index');
        $api->delete('/assets/{id}','AssetsController@destroy');
    });

    $api->group(['prefix'=>'oauth'],function($api){
        $api->post('token','\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');
    });
    // $api->group(['namespace'=>'App\Http\Controllers','middleware'=>['auth:api']],function($api){
    //     // Products
    //     $api->post('/products','ProductController@create');
    //     $api->get('/products','ProductController@index');
    //     $api->get('/products/{id}','ProductController@show');
    //     $api->put('/products/{id}','ProductController@update');
    //     $api->delete('/products/{id}','ProductController@destroy');
    // });
});