<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(@OA\Xml(name="Product"))
 */
class Product extends Model
{

    Use Uuid;

    protected $table = 'products';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price','description','status',
    ];
       
    public $incrementing = false;

    protected $keyType = 'uuid';
    /**
     * @OA\Property(
     *     description="Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *     format="float64",
     *     description="Price",
     *     title="price",
     * )
     *
     * @var float
     */
    public $price;

    /**
     * @OA\Property(
     *     description="Description",
     *     title="description",
     * )
     *
     * @var string
     */
    public $description;   

    /**
     * @OA\Property(
     *     description="Status",
     *     title="status",
     * )
     *
     * @var int
     */
    public $status;    
} 