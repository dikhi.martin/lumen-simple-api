<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Laravel\Passport\Token;

class PurgeExpiredTokensCommand extends Command
{
    protected $signature = 'passport:purge';

    protected $description = 'Delete expired OAuth access tokens';

    public function handle()
    {
        $deletedCount = Token::where('expires_at', '<', Carbon::now())->delete();

        $this->info("Deleted {$deletedCount} expired tokens.");
    }
}
