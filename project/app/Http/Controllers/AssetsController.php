<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File; 
use Spatie\QueryBuilder\QueryBuilder;
use App\Traits\RespondsWithHttpStatus;
use App\Models\Asset;
use App\Models\Product;


class AssetsController extends Controller
{
    
    protected  $root_path = 'assets';

    use RespondsWithHttpStatus;

    /**
    *    @OA\Post(path="/api/assets",
    *        tags={"Assets"},
    *        summary="Upload new assets",
    *        description="Upload new assets",
    *        operationId="uploadAssets",
    *        @OA\RequestBody(
    *           required=true,
    *           @OA\MediaType(
    *               mediaType="multipart/form-data",
    *               @OA\Schema(
    *                    @OA\Property(
    *                        description="File",
    *                        property="file",
    *                        type="file"
    *                    ),
    *                    @OA\Property(
    *                        description="Title",
    *                        property="title",
    *                        type="string",
    *                        format="string"
    *                    ),
    *                    @OA\Property(
    *                        description="Sub Folder",
    *                        property="sub_folder",
    *                        type="string",
    *                        format="string"
    *                    ),
    *               )
    *           )
    *        ),     
    *        security={
    *            {"api_key": {}}
    *        },
    *     @OA\Response(response="201", description="successful operation"),
    *     @OA\Response(
    *         response=401,
    *         description="Unauthorized",
    *         @OA\Schema(
    *             additionalProperties={
    *                 "type": "integer",
    *                 "format": "int32"
    *             }
    *         )
    *     ),
    *     @OA\Response(
    *         response=404,
    *         description="File not found",
    *         @OA\Schema(
    *             additionalProperties={
    *                 "type": "integer",
    *                 "format": "int32"
    *             }
    *         )
    *     ),
    *     @OA\Response(
    *         response=422,
    *         description="Unprocessable Content",
    *         @OA\Schema(
    *             additionalProperties={
    *                 "type": "integer",
    *                 "format": "int32"
    *             }
    *         )
    *     )
    *    )
    */    
    public function create(Request $request){
        $this->validate($request, [
            "title" => "required",
        ]);
        
        if (!$request->hasFile('file')) {
            return $this->errorNotFound('File not found');
        }
        
        $file = $request->file('file');
        $original_file_name = $file->getClientOriginalName();
        $file_ext = $file->getClientOriginalExtension();
        $file_size = $file->getSize();
        $filename = 'P-'.time().'-'.Str::random(10) . '.' . $file_ext;
        
        $path = $this->root_path;
        if($request->sub_folder){
            $path = 'assets/'.$request->sub_folder;
        }
        $destination_path = './'. $path .'/';
        $relative_path = $path.'/'.$filename;
        $absolute_path = env("APP_URL").'/'.$path.'/'.$filename;
        if (!$file->move($destination_path, $filename)) {
            return $this->errorInternal('Cannot upload file');
        }
        
        $data = new Asset();
        $data->title = $request->title;
        $data->file_name = $filename;
        $data->file_size = $file_size;
        $data->original_file_name = $original_file_name;
        $data->absolute_path = $absolute_path;
        $data->relative_path = $relative_path;
        $data->status = 1;
        $data->save();

        return $this->created($data, null);
    }

    /**
     * @OA\Get(path="/api/assets",
     *     tags={"Assets"},
     *     summary="Get data assets",
     *     description="Get data assets",
     *     operationId="getAssets",
     *     @OA\Parameter(
     *         name="filter[title]",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),     
     *     @OA\Parameter(
     *         name="filter[file_name]",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),     
     *     @OA\Parameter(
     *         name="filter[original_file_name]",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),     
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number start from one",
     *         required=false,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="size",
     *         in="query",
     *         description="Size per page, default 15",
     *         required=false,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="sort",
     *         in="query",
     *         description="Sort by field, adding dash (-) at the beginning means descending and vice versa",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),     
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     security={
     *        {"api_key": {}}
     *     },
     * )
     */
    public function index(Request $request){
        $res = QueryBuilder::for(Asset::class)
        ->allowedFilters(['title','file_name', 'original_file_name'])
        ->defaultSort('-created_at')
        ->allowedSorts(['title','-title','file_size','-file_size','file_name','-file_name'])
        ->paginate($request->input('size'))
        ->appends(request()->query());
        return response()->json($res);
    }


        /**
     * @OA\Delete(
     *     path="/api/assets/{id}",
     *     tags={"Assets"},
     *     summary="Deleted a assets",
     *     description="Deleted a single assets by ID",
     *     operationId="deleteAssets",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Assets id to delete",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             format="uuid"
     *         ),
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid ID assets",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Assets not found",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation"
     *     ),
     *     security={
     *        {"api_key": {}}
     *     },
     * )
     */
    public function destroy($id){
        $res = Asset::find($id);
        if (!$res) {
            return $this->errorNotFound("record not found");
        }
        if(!File::exists(public_path($res->relative_path))){
            return $this->errorNotFound("file not found");
        }
        File::delete(public_path($res->relative_path));
        $res->delete();
        
        return $this->deleted("Data deleted successfully");
    }

    

}
