<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Traits\RespondsWithHttpStatus;
use Spatie\QueryBuilder\QueryBuilder;


class ProductController extends Controller
{
    use RespondsWithHttpStatus;
    /**
    *    @OA\Post(path="/api/products",
    *        tags={"Product"},
    *        summary="Create new product",
    *        description="Create new product",
    *        operationId="createProduct",
    *        @OA\RequestBody(
    *           required=true,
    *           @OA\MediaType(
    *               mediaType="application/json",
    *                @OA\Schema(ref="#/components/schemas/Product")
    *           )
    *        ),      
    *        security={
    *            {"api_key": {}}
    *        },
    *     @OA\Response(response="201", description="successful operation"),
    *     @OA\Response(
    *         response=401,
    *         description="Unauthorized",
    *         @OA\Schema(
    *             additionalProperties={
    *                 "type": "integer",
    *                 "format": "int32"
    *             }
    *         )
    *     )
    *    )
    */    
    public function create(Request $request){
        $this->validate($request, [
            "name" => "required|unique:products",
            "price" => "required",
            "status" => "required"
        ]);

        $data = $request->all();
        $res = Product::create($data);

        return $this->created($res, null);
    }
    
    /**
     * @OA\Get(path="/api/products",
     *     tags={"Product"},
     *     summary="Get data product",
     *     description="Get data product",
     *     operationId="getProduct",
     *     @OA\Parameter(
     *         name="filter[name]",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),     
     *     @OA\Parameter(
     *         name="filter[price]",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number"
     *         )
     *     ),     
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number start from one",
     *         required=false,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="size",
     *         in="query",
     *         description="Size per page, default 15",
     *         required=false,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="sort",
     *         in="query",
     *         description="Sort by field, adding dash (-) at the beginning means descending and vice versa",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     security={
     *        {"api_key": {}}
     *     },
     * )
     */
    public function index(Request $request){
        $res = QueryBuilder::for(Product::class)
        ->allowedFilters(['name', 'price'])
        ->defaultSort('-created_at')
        ->allowedSorts(['price','-price'])
        ->paginate($request->input('size'))
        ->appends(request()->query());

        return response()->json($res);
    }

    /**
     * @OA\Get(
     *     path="/api/products/{id}",
     *     tags={"Product"},
     *     summary="Find product by ID",
     *     description="Returns a single product",
     *     operationId="getProductById",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of product to return",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             format="uuid"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/Product"),
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid ID product"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Product not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     security={
     *        {"api_key": {}}
     *     }
     * )
     *
     * @param string $id
     */
    public function show($id){
        $res = Product::find($id);
        if($res == null){
            return $this->errorNotFound(null);
        }
        return response()->json($res);
    }

    /**
     * @OA\Put(
     *     path="/api/products/{id}",
     *     tags={"Product"},
     *     summary="Update product by ID",
     *     description="Update a single product by ID",
     *     operationId="updateProductByID",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of product",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             format="uuid"
     *         )
     *     ),
     *     @OA\RequestBody(
     *           required=true,
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                @OA\Schema(ref="#/components/schemas/Product")
     *           )
     *     ),      
     *     security={
     *          {"oauth2": {}}
     *     },
     *     @OA\Response(
     *         response=400,
     *         description="Invalid ID product"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Product not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Validation exception"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     * 
     *     @OA\Response(
     *         response=200,
     *         description="successful operation"
     *     ),
     *     security={
     *        {"api_key": {}}
     *     },
     *     @OA\RequestBody(ref="#/components/requestBodies/Product")
     * )
     */
    public function update(Request $request, $id){
        $res = Product::find($id);
        if (!$res) {
            return $this->errorNotFound(null);
        }
        
        $this->validate($request, [
            "name" => "required",
            "price" => "required",
            "status" => "required"
        ]);

        $data = $request->all();
        $res->fill($data);
        $res->save();

        return $this->ok($res, null);
    }
    

    /**
     * @OA\Delete(
     *     path="/api/products/{id}",
     *     tags={"Product"},
     *     summary="Deleted a product",
     *     description="Deleted a single product by ID",
     *     operationId="deleteProduct",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Product id to delete",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             format="uuid"
     *         ),
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid ID product",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Product not found",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation"
     *     ),
     *     security={
     *        {"api_key": {}}
     *     },
     * )
     */
    public function destroy($id){
        $res = Product::find($id);
        if (!$res) {
            return $this->errorNotFound(null);
        }
        $res->delete();
        
        return $this->deleted("Data deleted successfully");
    }
} 

