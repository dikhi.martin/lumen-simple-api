<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserController extends Controller
{

    /**
    *    @OA\Post(path="/api/register",
    *        tags={"User"},
    *        summary="Registration",
    *        description="Registration",
    *        @OA\RequestBody(
    *           required=true,
    *           @OA\MediaType(
    *               mediaType="multipart/form-data",
    *               @OA\Schema(
    *                    @OA\Property(
    *                        description="Email",
    *                        property="email",
    *                        type="string",
    *                        format="email",
    *                    ),
    *                    @OA\Property(
    *                        description="Password",
    *                        property="password",
    *                        type="string",
    *                        format="string",
    *                        maximum="255",
    *                    ),
    *               )
    *           )
    *        ),      
    *        security={
    *            {"api_key": {}}
    *        },
    *        @OA\Response(response="default", description="successful operation")
    *    )
    */
    public function register(Request $request){
        $this->validate($request, [
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6'
        ]);

        $email = $request->input('email');
        $password = Hash::make($request->input('password'));

        $user = User::create([
            'email' => $email,
            'password' => $password
        ]);

        return response()->json([
            'status' => 201,
            'message' => 'Created'
        ], 201);
    }
    
    /**
    *    @OA\Post(path="/api/oauth/token",
    *        tags={"User"},
    *        summary="Generate token",
    *        description="Generate token",
    *        @OA\RequestBody(
    *        required=true,
    *           @OA\MediaType(
    *               mediaType="application/json",
    *               @OA\Schema(
    *                    @OA\Property(
    *                        description="Grant type",
    *                        property="grant_type",
    *                        example="password",
    *                        type="string",
    *                        format="string",
    *                    ),
    *                    @OA\Property(
    *                        description="Client ID",
    *                        property="client_id",
    *                        example="4",
    *                        type="string",
    *                        format="string",
    *                    ),
    *                    @OA\Property(
    *                        description="Client Secret",
    *                        property="client_secret",
    *                        example="RRE2ZuxPpFUVFi1kzYV1u2fStMsLZ7supcPOnxMn",
    *                        type="string",
    *                        format="string",
    *                    ),
    *                    @OA\Property(
    *                        description="Username",
    *                        property="username",
    *                        type="string",
    *                        example="sample@mail.com",
    *                        format="string",
    *                    ),
    *                    @OA\Property(
    *                        description="Password",
    *                        property="password",
    *                        example="password",
    *                        type="string",
    *                        format="int64",
    *                        maximum="255",
    *                    ),
    *               )
    *           )
    *        ),      
    *        security={
    *            {"api_key": {}}
    *        },
    *        @OA\Response(response="default", description="successful operation")
    *    )
    */
    public function login(Request $request){
        return response()->json(200);
    }
} 