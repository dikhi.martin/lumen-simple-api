<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Simple API Lumen",
 *      description="Simple API Lumen 8.3.1",
 *      @OA\Contact(
 *          name="dikhi.martin@gmail.com",
 *          email="dikhi.martin@gmail.com"
 *      ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 * @OA\SecurityScheme(
 *     name="bearerAuth",
 *     securityScheme="bearerAuth",
 *     type="http",
 *     scheme="bearer",
 *     description="Basic Auth"
 * )
 * @OA\SecurityScheme(
 *     type="apiKey",
 *     in="header",
 *     securityScheme="api_key",
 *     name="x-Token"
 * )
 * @OA\SecurityScheme(
 *     type="oauth2",
 *     name="oauth2",
 *     securityScheme="oauth2",
 *     description="Oauth2 security",
 *     bearerFormat="bearer",
 *     @OA\Flow(
 *         flow="password",
 *         tokenUrl="http://localhost:8000/api/oauth/token",
 *         scopes={*}
 *     )
 * )
 */
class Controller extends BaseController
{
    //
}
