<?php

namespace App\Http\Middleware;
use App\Traits\RespondsWithHttpStatus;

use Closure;

class TokenMiddleware
{
    use RespondsWithHttpStatus;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        if(!isset($_SERVER['HTTP_X_TOKEN'])){  
            return $this->unauthorized("Please set x-Token header");
        }  
        
        if($_SERVER['HTTP_X_TOKEN'] != env("TOKEN_KEY")){  
            return $this->unauthorized("Wrong x-Token header");
        }  
        return $next($request);
    }
}
